/* Author: 
    Ninad Parkar
*/
var hamburger = document.querySelector('.hamburger'), //For Hamburger Icon in mobile view
sortBtn = document.querySelector('.filter_menu').children, //For selecting all li of class filter_menu
sortItem = document.querySelector('.filter_item').children, //For selecting all li of class filter_item
a = document.querySelectorAll(".images a"), //For selecting anchor tags situated under class images
modal = document.getElementById("myModal"), //For selecting Modal
close = document.getElementById("close"), //For selecting close icon of modal
modalContent = document.getElementById("modal_content"), //To store what to show on modal
i, j, k, //For looping
menuLength = sortBtn.length, //For getting length of li of filter_menu
itemLength = sortItem.length, //For getting length of li of filter_item
nav = document.querySelector('nav'); //For selecting Navbar

//For adding preventDeafault for all a tag in class images
for (i = 0; i < a.length; i++) {
    a[i].onclick = (e) => e.preventDefault(); 
}

//For Filtering the Images
for(i = 0; i < menuLength; i++){
    sortBtn[i].addEventListener('click', function(){
        for(j = 0; j< menuLength; j++){
            sortBtn[j].classList.remove('current');
        }

        this.classList.add('current');
        
        var targetData = this.getAttribute('data-target');

        for(k = 0; k < itemLength; k++){
            sortItem[k].classList.remove('active');
            sortItem[k].classList.add('delete');

            if(sortItem[k].getAttribute('data-item') == targetData || targetData == "all"){
                sortItem[k].classList.remove('delete');
                sortItem[k].classList.add('active');
            }
        }
    });
}

//For Modal
for(i = 0; i < itemLength; i++){
    sortItem[i].addEventListener('click', function(){
        imgSrc = (((this).childNodes[1]).childNodes[1]).childNodes[0].src;
        modal.style.display = "block";
        modalContent.src = imgSrc;
        document.querySelector('body').style.position = 'fixed';
    });
}

//For closing the Modal
close.onclick = function() {
    modal.style.display = "none";
    document.querySelector('body').style.position = 'static';
}

//For hamburger icon to toggle menu in mobile view
hamburger.onclick = function() {
    if(nav.style.display == 'block') {
        nav.style.display = 'none';
    }
      else {
        nav.style.display = 'block';
    }
}